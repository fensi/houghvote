#include "imageobj.h"
#include "params.h"

ImageObj::~ImageObj(){

}

void ImageObj::initialization(){
  if ( _FEAT_TYPE == 0 ) {
    feat_type_ = "hesaff"; 
  }else if (_FEAT_TYPE == 1 )
    feat_type_ = "harlap";

  if ( _DESC_TYPE == 0 ) {
    desc_type_ = "opponent_sift";
  }else if ( _DESC_TYPE == 1 ){
    desc_type_ = "sift" ; 
  }else{

  }

  if (_DISTANT_TYPE == 0  ) {
   distance_type_ = "chi_square"; 
  }else if (_DISTANT_TYPE  == 1 ) {
    distance_type_ = "euclidean";
  }else {

  }

  if ( _REGION_TYPE == 0) {
    region_type_ = "aff_region"; }
  else if ( _REGION_TYPE == 1 ) {
    region_type_ = "DOG_region";
  }
  sz_patch     = _PATCHSIZE ;  
  scaling      = _FEATURESCALE;
  if ( _BESTIMATEORIENTATION == true ) {
    n_max_orientation   = _NMAXORI;
  }else{
    n_max_orientation   = 0;
  }

  thres_dist                          = _THRES_DIST;
  knn                                   = _KNN;
  radius_nn                          = _RADIUS; 
  b_match_distribution        = _BESTIMATEORIENTATION; 
  redundancy_threshold      = _REDUNDANCY_THRES  ;
  b_filter_match                   = _BFILTERMATCH;
  b_resample_ptch               = _BRESAMPLEPTCH;
  b_use_flann                       = false;;
  
  if ( b_resample_ptch == 0 )
       n_max_orientation = 0;
}

void ImageObj::load_images(){
  img_ref = imread(mReferenceImgPath.c_str(), IMREAD_COLOR); 
  img_tar = imread(mTargetImgPath.c_str(), IMREAD_COLOR);
  if (img_ref.empty() ){ 
    cout<< "could not open input image: "<< mReferenceImgPath.c_str() << endl; 
    exit(0);
  }
  if (img_tar.empty() ){ 
    cout<< "could not open input image: "<< mTargetImgPath.c_str()<< endl; 
    exit(0);
    }
}

/* This function extract features from image and compute affine transformation for each feature 
Input:
Output:  feat           - x y a b c oriendation scale 
   affmatrix  - 3x3 matrix
   patch        - patchSize * patchSize * 3 
*/
void ImageObj::extract_feature(string mImagePath, Mat& img,  vector<Mat>& affmatrix, vector<EllipseRegion>& ellipse_corners, Mat& feat_desc, vector<int>& feat_label){
  
  vector<EllipseRegion> ellipse_corners_ori; 
 
  //string str = mImagePath + "." + feat_type_ + "." + desc_type_;
  string str = mImagePath + "." + feat_type_ + "." + "sift";
  int hres_threshold = 400;
  fstream f(str.c_str()); 
  if (!f) { // check if feature_file is already there
    stringstream ss; 
    ss << "./bin/extract_features_64bit.ln" << " -" << feat_type_ << " -" << "sift" << " -hesThres "<< hres_threshold <<" -i "<< mImagePath;
    system(ss.str().c_str());
    //this->load_feature_from_file(mImagePath, ellipse_corners_ori);
    this->load_feature_from_file(mImagePath, ellipse_corners_ori, feat_desc); 
    cout<<"loading: "<<ellipse_corners_ori.size()<<" features, dim: "<<feat_desc.cols<<endl;
  }else{
    //this->load_feature_from_file(mImagePath, ellipse_corners_ori);
    this->load_feature_from_file(mImagePath, ellipse_corners_ori, feat_desc); 
    cout<<"loading: "<<ellipse_corners_ori.size()<<" features, dim: "<<feat_desc.cols<<endl;
  } 
  f.close();

  /*  Acqure region patches and descriptor from features*/
  cout<<"Start to extract feature transformation, patch information from detected image regions..."<<endl;
 
  /*-------------------make transformation from region features -------------------*/
  extract_affine_transform(ellipse_corners_ori, img, affmatrix, ellipse_corners, feat_desc, feat_label); 
 
}

void ImageObj::extract_affine_transform(const vector<EllipseRegion>& ellipse_corners_ori,  Mat& img, vector<Mat>& affmatrix,  vector<EllipseRegion>& ellipse_corners, Mat& feat_desc, vector<int>& feat_label){
   vector<EllipseRegion> ellipse_corners_purify = purify_feature(ellipse_corners_ori); 
   vector<int> feat_idx;  // 
   vector<EllipseRegion> feat_aug; 
   vector<float> angle; 
   vector<bool> out_of_img; 
   vector<Mat> affmatrix_ori; 
  vector<Mat> feat_patch_ori; 

  cout<<"ellipse_corners_purify.size(): "<<ellipse_corners_purify.size()<<endl;
   if ( b_resample_ptch) {
    if (region_type_ == "aff_region")      
        compute_norm_trans_image(ellipse_corners_purify, img,  affmatrix_ori, feat_idx, feat_aug, angle);
    else if (region_type_ == "DOG_region") {
       from_DOG_to_ellipse(ellipse_corners_purify);
       from_DOG_to_trans(ellipse_corners_purify, affmatrix_ori, feat_idx, feat_aug, angle);
    }
    cout<<"computing transformation for #"<< feat_aug.size() <<" features finished."<<endl;
    /* Reintialize feature information */ 
    int n_feat = feat_idx.size();
    ellipse_corners_purify.resize(n_feat, EllipseRegion(0,0,0,0,0));
    for (int i = 0 ; i < n_feat ; i++){
      ellipse_corners_purify.at(i).ellipse_u   = feat_aug.at(i).ellipse_u; 
      ellipse_corners_purify.at(i).ellipse_v   = feat_aug.at(i).ellipse_v; 
      ellipse_corners_purify.at(i).ellipse_a   = feat_aug.at(i).ellipse_a; 
      ellipse_corners_purify.at(i).ellipse_b   = feat_aug.at(i).ellipse_b; 
      ellipse_corners_purify.at(i).ellipse_c    = feat_aug.at(i).ellipse_c; 
      ellipse_corners_purify.at(i).ellipse_ori = angle.at(i); 
    } 
    /* Obtain local patch using features */ 
    feat_patch_ori.reserve(n_feat);
    out_of_img.reserve(n_feat);
    for (int i_feat = 0 ; i_feat < n_feat; i_feat++) {
      Mat norm_region(sz_patch, sz_patch, CV_8UC3) ; 
      bool b_outside                    = normalize_patch(img, affmatrix_ori.at(i_feat), norm_region);   
      feat_patch_ori.push_back(norm_region);
      out_of_img.push_back(b_outside);    
    }
    /* Delete features which are out of images*/
    vector<Mat> feat_patch  = erase_element_from_array_bool(feat_patch_ori, out_of_img);
    ellipse_corners               = erase_element_from_array_bool(ellipse_corners_purify, out_of_img); 
    affmatrix                       = erase_element_from_array_bool(affmatrix_ori, out_of_img); 
    feat_desc                        = generate_descriptor(feat_patch);  
  }else{
     if (region_type_ == "aff_region")      
       compute_norm_trans_image(ellipse_corners_ori, img,  affmatrix_ori, feat_idx, feat_aug, angle);
    else if (region_type_ == "DOG_region") 
       from_DOG_to_trans(ellipse_corners_ori, affmatrix_ori, feat_idx, feat_aug, angle);

    affmatrix          = affmatrix_ori;
    ellipse_corners = ellipse_corners_ori; 
  }
  feat_label                       = generate_feat_label(ellipse_corners); 
  
  //draw_features(img, ellipse_corners);  
  /* free memory*/
  feat_idx.clear();
  feat_aug.clear();
  angle.clear();
  out_of_img.clear();
  vector<int>(feat_idx).swap(feat_idx);
  vector<EllipseRegion>(feat_aug).swap(feat_aug);
  vector<float>(angle).swap(angle);
  vector<bool>(out_of_img).swap(out_of_img);  
}

void ImageObj::draw_features(const vector<EllipseRegion>& features, Mat& srcImg){
  RNG rng(12345);
  for (size_t i = 0 ; i < features.size(); i++){
    Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );   
    Point pt1(  features.at(i).ellipse_u, features.at(i).ellipse_v ); 
    circle( srcImg,  pt1, 3, color, 2, 8, 0); 
  }
  imshow( "Feature" , srcImg);
  waitKey(0); 
}

void ImageObj::save_features( const vector<EllipseRegion>& features, Mat srcImg, string filename){
  RNG rng(12345);
  for (size_t i = 0 ; i < features.size(); i++){
    Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );   
    Point pt1(  features.at(i).ellipse_u, features.at(i).ellipse_v ); 
    circle( srcImg,  pt1, 3, color, 2, 8, 0); 
  }
  imwrite( filename.c_str() , srcImg);   
}

void ImageObj::from_DOG_to_ellipse(vector<EllipseRegion>& features){
      
      for (auto ellipse: features){
         ellipse.ellipse_u = ellipse.ellipse_u ;
         ellipse.ellipse_v = ellipse.ellipse_v ;
         ellipse.ellipse_a =  0.05/(ellipse.ellipse_a*ellipse.ellipse_a) ;
         ellipse.ellipse_b = 0; 
         ellipse.ellipse_c =  0.05/(ellipse.ellipse_c * ellipse.ellipse_c) ;
      }
}

/* 
This function load features from file and put  feature into ellipse_corners 
*/
//void ImageObj::load_feature_from_file(string mImagePath, vector<EllipseRegion>& ellipse_corners){
void ImageObj::load_feature_from_file(string mImagePath, vector<EllipseRegion>& ellipse_corners, Mat& feat_desc){
  
  //string loadFileName = mImagePath + "." + feat_type_ + "." + desc_type_ ;
  string loadFileName = mImagePath + "." + feat_type_ + "." + "sift";
  float ellipse_u =0, ellipse_v = 0 , ellipse_a = 0 , ellipse_b = 0 , ellipse_c = 0 ; 
  std::ifstream  ifs(loadFileName.c_str()); 
  int dim, num_feat; 
  std::string line;
  int f_idx = 0;
  if (ifs.is_open()) {    
    std::getline(ifs, line); 
    stringstream sd(line);
    sd >> dim; 
    std::getline(ifs, line); 
    stringstream sf(line);
    sf  >> num_feat;  
    feat_desc.create(num_feat, dim, CV_32F); 
    ellipse_corners.reserve(num_feat);
    while ( std::getline(ifs, line) ){  
       stringstream ss(line);
       ss >> ellipse_u >> ellipse_v >> ellipse_a >> ellipse_b >> ellipse_c; 
       ellipse_corners.push_back(EllipseRegion(ellipse_u,ellipse_v,ellipse_a,ellipse_b,ellipse_c));      
       for (size_t dim_idx = 0; dim_idx < dim; dim_idx++){
              ss >> feat_desc.at<float>(f_idx, dim_idx);
       }
       f_idx++;
    }
  }else{    
    cout<<"can not open file:  "<< loadFileName.c_str()<< endl;
    exit(1);
  }
  ifs.close();
}
/* 
This function extract orienated region from feature
Input: region feature (ellipse_vec)
Output:  feat_idx, feat_aug: , affmatrix: ,  angle: 
*/
void ImageObj::compute_norm_trans_image(const vector<EllipseRegion>& ellipse_vec, Mat& img,  vector<Mat>& affmatrix, vector<int>& feat_idx, vector<EllipseRegion>& feat_aug, vector<float>& angle){
  int n_feat                    = ellipse_vec.size();  // number of features 
  int i_aug_feat              = 0 ; 
  float fixed_angle = 0 ; 
  int n_buff                   = n_feat * std::max(1, n_max_orientation); 
  affmatrix.reserve(n_buff);
  feat_idx.reserve(n_buff);
  feat_aug.reserve(n_buff); 
  angle.reserve(n_buff);
  int count = 0; 

  for (int i_feat = 0 ; i_feat < n_feat ; i_feat++) {    
    Mat A_ori = (Mat_<float>(2,2) << ellipse_vec.at(i_feat).ellipse_a, ellipse_vec.at(i_feat).ellipse_b, ellipse_vec.at(i_feat).ellipse_b,  ellipse_vec.at(i_feat).ellipse_c);   
    Mat A                                =  take_power(A_ori, -0.5); 
    Mat t_affmatrix = ( Mat_<float>(3,3) << A.at<float>(0,0),  A.at<float>(0,1), 
                                                                              ellipse_vec.at(i_feat).ellipse_u, A.at<float>(1,0),  A.at<float>(1,1), ellipse_vec.at(i_feat).ellipse_v, 
                                                                              0, 0, 1);    
    if ( n_max_orientation  <= 0 ) {      
      i_aug_feat                           = i_aug_feat + 1; 
      Mat R = (Mat_<float>(2,2) << cos(fixed_angle),  -sin(fixed_angle), sin(fixed_angle), cos(fixed_angle) );
      Mat AR                                     = A*R;     
      Mat aff_mat = ( Mat_<float>(3,3) <<  AR.at<float>(0,0), AR.at<float>(0,1), 
                                                                          ellipse_vec.at(i_feat).ellipse_u,  AR.at<float>(1,0),  AR.at<float>(1,1), ellipse_vec.at(i_feat).ellipse_v, 
                                                                          0,0,1);
      affmatrix.push_back(aff_mat);
      feat_idx.push_back(i_feat);
      feat_aug.push_back(EllipseRegion(ellipse_vec.at(i_feat).ellipse_u,ellipse_vec.at(i_feat).ellipse_v,ellipse_vec.at(i_feat).ellipse_a,ellipse_vec.at(i_feat).ellipse_b,ellipse_vec.at(i_feat).ellipse_c));
      angle.push_back(fixed_angle); 
    }else{
      //Normalize patch
      Mat t_norm_region(sz_patch, sz_patch, CV_8UC3);
      bool b_outside = normalize_patch(img, t_affmatrix, t_norm_region);
      if ( b_outside ) { 
        count = count + 1;
        continue;
      } 
      //compute dominant orientation
      vector<float> t_angle = dominant_orientation(t_norm_region);  
      for (size_t iAngle = 0 ; iAngle < t_angle.size(); iAngle++) {
        i_aug_feat    = i_aug_feat + 1;         
        Mat R = (Mat_<float>(2,2) << cos(t_angle.at(iAngle)),  -sin(t_angle.at(iAngle)), sin(t_angle.at(iAngle)), cos(t_angle.at(iAngle)) );       
        Mat AR       = A*R;
        Mat aff_mat = ( Mat_<float>(3,3) <<  AR.at<float>(0,0), AR.at<float>(0,1), ellipse_vec.at(i_feat).ellipse_u,  AR.at<float>(1,0),  AR.at<float>(1,1), ellipse_vec.at(i_feat).ellipse_v, 0,0,1);
        affmatrix.push_back(aff_mat);
        feat_idx.push_back(i_feat);
        feat_aug.push_back(EllipseRegion(ellipse_vec.at(i_feat).ellipse_u,ellipse_vec.at(i_feat).ellipse_v,ellipse_vec.at(i_feat).ellipse_a,ellipse_vec.at(i_feat).ellipse_b,ellipse_vec.at(i_feat).ellipse_c));
        angle.push_back(t_angle.at(iAngle) ); 
      }
      
    } 
  }
  
  if  ( i_aug_feat < n_buff ) {  
    feat_idx.erase(feat_idx.begin() + i_aug_feat, feat_idx.end());
    feat_aug.erase(feat_aug.begin() + i_aug_feat, feat_aug.end() );
    angle.erase(angle.begin() + i_aug_feat, angle.end());
    affmatrix.erase(affmatrix.begin() + i_aug_feat, affmatrix.end());
  }
} 

/* 
This function normalize the input patch (mxImg) using input transformation (affmatrix)
Input: affmatrix , mxImg
Output: t_norm_region 
*/
bool ImageObj::normalize_patch(const Mat& mxImg, const Mat& affmatrix, Mat& t_norm_region){
  /* Read transform data from tr_matrix,  considering scale factor  */
  float sc                            = 2.0 * (float)scaling/(float)sz_patch;  // because patchSize is a diameter (not radius)
  float  x                            = affmatrix.at<float>(0,2);
  float  y                            = affmatrix.at<float>(1,2);
  // initialization matrix  
  Mat matrix_a = ( Mat_<float>(2,2) << affmatrix.at<float>(0,0) * sc, affmatrix.at<float>(0,1) * sc , affmatrix.at<float>(1,0) * sc,affmatrix.at<float>(1,1) * sc );
 
  Mat matrix_pt(1,2,CV_32F);
  Mat matrix_pts(1,2, CV_32F); //grid coordinate 
  /* Comptue half size. Get image size */
  int half_size                      = floor( sz_patch/2.0); 
  int height               = mxImg.rows;
  int width                = mxImg.cols;
  int chan                 = mxImg.channels();
  bool b_outside                = 0;
  cv::Mat_<cv::Vec3b>::iterator it        = t_norm_region.begin<cv::Vec3b>();
  cv::Mat_<cv::Vec3b>::iterator itend = t_norm_region.end<cv::Vec3b>(); 
  /* Compute the transformed region with bilinear interpolation*/
  for (int pos_x =  -half_size ; pos_x <= half_size ; pos_x++) { // 
    for (int pos_y = -half_size ; pos_y <= half_size; pos_y++) {
      matrix_pt.at<float>(0,0)   = matrix_a.at<float>(0,0) * (float)pos_x + matrix_a.at<float>(0,1)* (float)pos_y;
      matrix_pt.at<float>(0,1)   = matrix_a.at<float>(1,0) * (float)pos_x + matrix_a.at<float>(1,1)* (float)pos_y;
      matrix_pts.at<float>(0,0) = floor(matrix_pt.at<float>(0,0));
      matrix_pts.at<float>(0,1) = floor(matrix_pt.at<float>(0,1)); 
      int xt         = (int) matrix_pts.at<float>(0,0);
      int yt                                  = (int) matrix_pts.at<float>(0,1); 
      float dx                          = matrix_pt.at<float>(0,0) - matrix_pts.at<float>(0,0);
      float dy                          = matrix_pt.at<float>(0,1) - matrix_pts.at<float>(0,1);
      int tmp_y                           = half_size + pos_y  ; 
      int tmp_x                           = half_size + pos_x ;
      
      if (  round(x + xt) > 0 &&  ( round(x+xt) + 1) < width && round(y + yt) > 0 && (round(y + yt) + 1) < height ){
        for (int k = 0; k < chan ; k++) {
          float v1= (float) mxImg.at<Vec3b>(round(y + yt )-1      ,round(x + xt)      -1)[k]  *(1.0-dx)*(1.0-dy);
          float v2= (float) mxImg.at<Vec3b>(round(y + yt +1) -1 ,round(x + xt)      -1)[k]  *(1.0-dx)*(dy) ;
          float v3=  (float) mxImg.at<Vec3b>(round(y + yt) - 1    ,round(x + xt +1) -1)[k]  *(dx)*(1.0-dy);
          float v4 = (float) mxImg.at<Vec3b>(round(y + yt +1)-1 ,round(x + xt +1 ) -1)[k] *(dx)*(dy);       
          t_norm_region.at<Vec3b>(tmp_y, tmp_x)[k] = saturate_cast<uchar>(v1+ v2 + v3 + v4);
        }       
      }else{
        for (int k = 0 ; k < chan ; k++) {
          t_norm_region.at<Vec3b>(tmp_y, tmp_x)[k] = 0; 
        }
        b_outside = true; 
        return b_outside;
      }     
    }
  } 
  return b_outside;
}

/* This function compute the doninant orientation of a input patch
Input   : t_norm_region (sz_patch * sz_patch * CV_32FC3)
Output: 
*/
vector<float> ImageObj::dominant_orientation(Mat& t_norm_region){
  Mat t_norm_region_gray ;
  if ( t_norm_region.channels() == 3 ) {
    cv::cvtColor(t_norm_region, t_norm_region_gray, CV_BGR2GRAY);
  }
  Mat t_norm_region_gray_norm; 
  t_norm_region_gray.convertTo( t_norm_region_gray_norm, CV_32F, 1.0/255.0); 
  Mat img_mag, img_dir;  
  compute_grad_mag(t_norm_region_gray_norm, 1.0, img_mag, img_dir); // compute gradient magnitude of t_norm_region  
  int n_suppress           = 2;   
  int bin_nb                  = 90;  //direction is form 0 ~ 90 
  vector<float> his; 
  his.resize(bin_nb,0); 
  /* Make img_dir in the range of [ 0, bin_nb ] */
  //Mat img_dir_tmp       = (bin_nb -1) * (img_dir + M_PI)*(1/(2*M_PI)) ;
  Mat img_dir_tmp       = (bin_nb -1) * (img_dir)*(1.0/(2.0*M_PI)) ;  
  float a            = (bin_nb-1) * 0.5; 
  Mat ttt             = img_dir_tmp + a; 
  Mat img_dir_v2          = take_floor(ttt);   
  img_dir_v2                  = img_dir_v2 +1;

  int height                   = t_norm_region.rows; 
  int width                    = t_norm_region.cols; 
  float radius           = height/5.0;
  float radius_2       = radius * radius ; 
  float half               = floor(height/2.0);
  for (int y = 0; y < height ; y++) {
    for ( int x = 0 ; x < width ; x++){
      float mag = (float)img_mag.at<float>(y,x); 
      int ori            = (int)img_dir_v2.at<float>(y,x); 
      if (  (pow( x+1 - half, 2 ) + pow(y+1 - half ,2 )) < radius_2) {
        his.at(ori-1) = his.at(ori-1) + mag; 
      }
    }
  }   
  // non-maximal suppresion around max 
  std:: vector<float>::iterator result = max_element(his.begin(), his.end()); 
  float maxValue                                 = *result;   
  int in                              = std::distance(his.begin(), result) ; // find the dominant direction
  vector<int> bin;   //dynamic construction
  bin.reserve(n_max_orientation);
  bin.push_back(in + 1);
  
  if (n_max_orientation > 1 ) {
    vector<int> round_bin ; round_bin.resize(2*bin_nb); 
    for (int i = 0 ; i < bin_nb ; i++ ){ 
      round_bin.at(i)                      = i + 1 ; 
      round_bin.at(bin_nb-1 + i + 1  ) = i + 1 ; 
    }
    his.at(in)       = 0 ; 
    vector<int> left(round_bin.begin()   + bin_nb + in - n_suppress, round_bin.begin() + bin_nb + in - 1  + 1 ) ;
    vector<int> right(round_bin.begin() + in + 1, round_bin.begin() +in + n_suppress + 1) ;
    set_vector_index_zero(his, left);
    set_vector_index_zero(his, right);
    
    for (int idx = 1 ; idx< n_max_orientation; idx++){
      result         = max_element(his.begin(), his.end()) ; 
      in                = std::distance(his.begin(), result) ; 
      float m  = *result; 
      if  ( m > maxValue*0.8){
        bin.push_back(in + 1 );
        his.at(in)      = 0;        
        left.assign(round_bin.begin()   + bin_nb + in - n_suppress,  round_bin.begin() + bin_nb + in - 1 + 1  );
        right.assign(round_bin.begin() + in + 1, round_bin.begin() +in + n_suppress + 1);
        set_vector_index_zero(his, left);
        set_vector_index_zero(his, right);
      }
    }
  }

  vector<float> tmp ; tmp.assign( bin.size(), 2 * M_PI / bin_nb) ; 
  vector<float> t_angle(bin.size());
  std::transform(bin.begin(), bin.end(), tmp.begin(), t_angle.begin(), std::multiplies<float>()); 
  return t_angle ; 
}

void ImageObj::from_DOG_to_trans(const vector<EllipseRegion>& ellipse_vec,vector<Mat>& affmatrix, vector<int>& feat_idx, vector<EllipseRegion>& feat_aug, vector<float>& angle ){
  int n_feat            = ellipse_vec.size(); 
  int n_buff            = n_feat * std::max(1, n_feat); 
  int i_aug_feat      = 0 ;
  int fixed_angle = 0; 
  feat_idx.reserve(n_buff);
  feat_aug.reserve(n_buff);
  angle.reserve(n_buff);
  affmatrix.reserve(n_buff);
  for (int i_feat = 0 ; i_feat < n_feat ; i_feat++){
    float scale                   = ellipse_vec.at(i_feat).ellipse_a; 
    float radius                 = 1./(scale*scale);       
    Mat A_ori = ( Mat_<float>(2,2) << radius, 0, 0, radius );
    Mat A                   = take_power(A_ori, -0.5); // inverser square root
    Mat R= ( Mat_<float>(2,2) <<cos(fixed_angle), -sin(fixed_angle), sin(fixed_angle),  cos(fixed_angle) );
    Mat AR                                     = A*R;
    Mat aff_mat = ( Mat_<float>(3,3)<<AR.at<float>(0,0), AR.at<float>(0,1),ellipse_vec.at(i_feat).ellipse_u,AR.at<float>(1,0),AR.at<float>(1,1), ellipse_vec.at(i_feat).ellipse_v, 0, 0,1 );
    i_aug_feat                           = i_aug_feat + 1;    
    feat_idx.push_back(i_feat);
    angle.push_back(fixed_angle);
    feat_aug.push_back(EllipseRegion(ellipse_vec.at(i_feat).ellipse_u,ellipse_vec.at(i_feat).ellipse_v,ellipse_vec.at(i_feat).ellipse_a,ellipse_vec.at(i_feat).ellipse_b,ellipse_vec.at(i_feat).ellipse_c));
    affmatrix.push_back(aff_mat);
  }
  if (i_aug_feat < n_buff){
    feat_idx.erase(feat_idx.begin() + i_aug_feat, feat_idx.end()) ; 
    feat_aug.erase(feat_aug.begin() + i_aug_feat, feat_aug.end());
    angle.erase(angle.begin() + i_aug_feat, angle.end());
    affmatrix.erase(affmatrix.begin() + i_aug_feat, affmatrix.end());
  }
}

/* extract descriptor from image patches */
Mat ImageObj::generate_descriptor(const vector<Mat>& feat_patch){
  int n_feat = feat_patch.size();  
  Mat feat_desc; 
  if ( desc_type_ == "opponent_sift" ) {
    feat_desc.create(n_feat, 128*3, CV_32F); 
    for ( int row_idx = 0; row_idx < n_feat ; row_idx++){   
        auto p = gensiftdesc_color( feat_patch.at( row_idx ) );
        p.copyTo( feat_desc.row( row_idx ) );
    }
    }else if (desc_type_ == "sift" ){
         feat_desc.create(n_feat, 128, CV_32F); 
          for ( int row_idx = 0; row_idx < n_feat ; row_idx++){   
            auto p = gensiftdesc( feat_patch.at( row_idx ) );
            p.copyTo( feat_desc.row( row_idx ) );
          }
    } 
  
  return feat_desc; 
}

Mat ImageObj::gensiftdesc(const Mat& patch_ori) {
  Mat feat_descriptor, ptch;

  cv::cvtColor(patch_ori, ptch, cv::COLOR_BGR2GRAY);
  ptch.convertTo(ptch, CV_32F, 1.0/255.0);
  float S              = 4;  
  int sz_1                  = ptch.rows;
  int width              = sz_1 - 2 ;
  float sigma2  = 3*width/16.0;      
  vector<float> c_x; 
  float delta      = width/(S + 1.0 ); 
  
  Mat xp, yp, cx, cy;
  vector<float> res_x ; 
  for (int i = 1; i <= width ; i++) res_x.push_back((float)i);         
  meshgrid(Mat(res_x), Mat(res_x),  xp, yp); 
  ///
  xp =xp.t();
  yp =yp.t();
  for (int i = 1 ; i <= S ; i ++ ) { c_x.push_back( (float)i * delta);  }
  meshgrid(Mat(c_x), Mat(c_x),  cx, cy);  
  int np                  = xp.rows * xp.cols; 
  Mat e2               = Mat::ones(1, S*S, CV_32F);
  Mat e1               = Mat::ones(np,1, CV_32F);

  //reshape xp --> xp_column, cx_column, yp_column, cy_column
  Mat xp_vec        = xp.reshape(0, xp.cols* xp.rows) ;  
  Mat yp_vec        = yp.reshape(0, yp.cols* yp.rows);
  Mat cx_vec          = cx.reshape(0, cx.cols* cx.rows);  
  Mat cy_vec          = cy.reshape(0, cy.cols* cy.rows);  
  Mat xpe2           = xp_vec*e2; 
  Mat ype2           = yp_vec*e2; 
  Mat e1cx           = e1*cx_vec.t(); 
  Mat e1cy           = e1*cy_vec.t();   
  // compute w 
  Mat w ; 
  Mat tmp1, tmp2, tmp3, tmp4;
  cv::subtract(xpe2, e1cx, tmp1); 
  cv::subtract(ype2, e1cy, tmp2);
  cv::pow(tmp1,2, tmp3); 
  cv::pow(tmp2,2, tmp4);
  Mat tmp5  = tmp3 + tmp4;
  Mat tmp6           = (-0.5)/(sigma2)* tmp5;
  cv::exp(tmp6 , w);  
  int nbins   = 8 ; 

  int sh        = 0; 
  vector<float> nbins_array; 
  for (int i = nbins ; i>=1 ; i--) { 
    float tmp = 2*M_PI/((float)nbins)* (i + sh) ;
    nbins_array.push_back(cos(tmp)) ;
    nbins_array.push_back(sin(tmp));
   }
  Mat cossin_ori(nbins_array); 
  Mat cossin = cossin_ori.reshape(0, nbins);
  Mat diffy    = ( Mat_<float>(3,3) << 1, 1, 1, 0, 0, 0, -1, -1, -1 ); 
  int n                = ptch.channels(); 
    Mat ip               = ptch.reshape(  1, sz_1);
    Mat conv2_ipx  = conv2(ip, diffy.t(), CONVOLUTION_SAME); 
    Mat conv2_ipy  = conv2(ip, diffy, CONVOLUTION_SAME); 
    Mat Ix                = conv2_ipx.reshape( n, sz_1);
    Mat Iy                = conv2_ipy.reshape( n, sz_1);    
    Mat Ix_Rect       = Ix(Rect(1,1, Ix.cols-2, Ix.rows-2));
    Mat Iy_Rect       = Iy(Rect(1,1, Iy.cols-2, Iy.rows-2));  
    Mat Ix_Rect_vec= static_cast<Mat>(Ix_Rect.t()).reshape( 0, Ix_Rect.rows * Ix_Rect.cols);    
    Mat Iy_Rect_vec= static_cast<Mat>(Iy_Rect.t()).reshape( 0, Iy_Rect.rows * Iy_Rect.cols);
    Mat v_cat_IxIy  ; 

    cv::hconcat(Ix_Rect_vec, Iy_Rect_vec, v_cat_IxIy);
    Mat B                  =static_cast<Mat>(v_cat_IxIy * cossin.t()).reshape( 0, n*np);
    // compare each element in B to a scalar ( 0 )
    Mat maxValue_B; 
    cv::max(B, Mat::zeros(B.rows, B.cols, B.type()), maxValue_B);
    Mat sift_ori         = static_cast<Mat>(w.t() * maxValue_B).reshape(n, S*S);    
    Mat sift                = static_cast<Mat>(sift_ori.reshape(0  ,S*S*nbins));  

    Mat sift_square; 
    cv::multiply(sift, sift, sift_square); 
    Scalar acc_gray  = sum(sift_square); 
    float  acc        = acc_gray[0];
    Mat sift_norm    = sift/sqrt(acc); 
    feat_descriptor.push_back(sift_norm);

  /* Release Mat*/
  return feat_descriptor.t(); 
}


/* generate sift descriptor from each patch
Input:     
Output:
*/
Mat ImageObj::gensiftdesc_color(const Mat& patch_ori){
  Mat feat_descriptor, patch;
  patch_ori.convertTo(patch, CV_32FC3, 1.0/255.0);
  float S              = 4;  
  int sz_1                  = patch.rows;
  int width              = sz_1 - 2 ;
  float sigma2  = 3*width/16.0;      
  vector<float> c_x; 
  float delta      = width/(S + 1.0 ); 
  /* make opponent sift*/
  vector<Mat> color_ptch(3), bgr; 
  cv::split(patch, bgr); 
  color_ptch.at(0) = (bgr[2] - bgr[1] )*(1.0/sqrt(2.0));                          // (R-G)/sqrt(2)
  color_ptch.at(1) = (bgr[2] - bgr[1] + 2.0* bgr[0]) * (1.0/sqrt(6.0)); // (R-G+2B)/sqrt(6)
  color_ptch.at(2) = (bgr[2] + bgr[1] + bgr[0])*(1.0/sqrt(3.0));  // (R+B+G)/sqrt(3)
  
  Mat xp, yp, cx, cy;
  vector<float> res_x ; 
  for (int i = 1; i <= width ; i++) res_x.push_back((float)i);         
  meshgrid(Mat(res_x), Mat(res_x),  xp, yp); 
  ///
  xp =xp.t();
  yp =yp.t();

  for (int i = 1 ; i <= S ; i ++ ) { c_x.push_back( (float)i * delta);  }
  meshgrid(Mat(c_x), Mat(c_x),  cx, cy);  
  int np                  = xp.rows * xp.cols; 
  Mat e2               = Mat::ones(1, S*S, CV_32F);
  Mat e1               = Mat::ones(np,1, CV_32F);
  //reshape xp --> xp_column, cx_column, yp_column, cy_column
  Mat xp_vec        = xp.reshape(0, xp.cols* xp.rows) ;  
  Mat yp_vec        = yp.reshape(0, yp.cols* yp.rows);
  Mat cx_vec          = cx.reshape(0, cx.cols* cx.rows);  
  Mat cy_vec          = cy.reshape(0, cy.cols* cy.rows);  
  Mat xpe2           = xp_vec*e2; 
  Mat ype2           = yp_vec*e2; 
  Mat e1cx           = e1*cx_vec.t(); 
  Mat e1cy           = e1*cy_vec.t();   
  // compute w 
  Mat w ; 
  Mat tmp1, tmp2, tmp3, tmp4;
  cv::subtract(xpe2, e1cx, tmp1); 
  cv::subtract(ype2, e1cy, tmp2);
  cv::pow(tmp1,2, tmp3); 
  cv::pow(tmp2,2, tmp4);
  Mat tmp5  = tmp3 + tmp4;
  Mat tmp6           = (-0.5)/(sigma2)* tmp5;
  cv::exp(tmp6 , w);  
  int nbins   = 8 ; 
  int sh        = 0; 
  vector<float> nbins_array; 
  for (int i = nbins ; i>=1 ; i--) { 
    float tmp = 2*M_PI/((float)nbins)* (i + sh) ;
    nbins_array.push_back(cos(tmp)) ;
    nbins_array.push_back(sin(tmp));
   }
  Mat cossin_ori(nbins_array); 
  Mat cossin = cossin_ori.reshape(0, nbins);
  Mat diffy    = ( Mat_<float>(3,3) << 1, 1, 1, 0, 0, 0, -1, -1, -1 ); 
  for (size_t chan = 0; chan < color_ptch.size() ; chan++ ){  
    Mat ptch           = color_ptch.at(chan);                 
    int n                = ptch.channels(); 
    Mat ip               = ptch.reshape(  1, sz_1);

    Mat conv2_ipx  = conv2(ip, diffy.t(), CONVOLUTION_SAME); 
    Mat conv2_ipy  = conv2(ip, diffy, CONVOLUTION_SAME); 
    Mat Ix                = conv2_ipx.reshape( n, sz_1);
    Mat Iy                = conv2_ipy.reshape( n, sz_1);    
    Mat Ix_Rect       = Ix(Rect(1,1, Ix.cols-2, Ix.rows-2));
    Mat Iy_Rect       = Iy(Rect(1,1, Iy.cols-2, Iy.rows-2));  
    Mat Ix_Rect_vec= static_cast<Mat>(Ix_Rect.t()).reshape( 0, Ix_Rect.rows * Ix_Rect.cols);    
    Mat Iy_Rect_vec= static_cast<Mat>(Iy_Rect.t()).reshape( 0, Iy_Rect.rows * Iy_Rect.cols);
    Mat v_cat_IxIy  ; 
    cv::hconcat(Ix_Rect_vec, Iy_Rect_vec, v_cat_IxIy);
    Mat B                  =static_cast<Mat>(v_cat_IxIy * cossin.t()).reshape( 0, n*np);
    // compare each element in B to a scalar ( 0 )
    Mat maxValue_B; 
    cv::max(B, Mat::zeros(B.rows, B.cols, B.type()), maxValue_B);
    Mat sift_ori         = static_cast<Mat>(w.t() * maxValue_B).reshape(n, S*S);    
    Mat sift                = static_cast<Mat>(sift_ori.reshape(0  ,S*S*nbins));  
    Mat sift_square; 
    cv::multiply(sift, sift, sift_square); 
    Scalar acc_gray  = sum(sift_square); 
    float  acc        = acc_gray[0];
    Mat sift_norm    = sift/sqrt(acc); 
    feat_descriptor.push_back(sift_norm);
  }
  /* Release Mat*/
  return feat_descriptor.t(); 
 }

void ImageObj::take_atan(const Mat& img_dx, const Mat& img_dy, Mat& output){

  output = Mat(img_dx.rows, img_dx.cols, CV_32F);
  for (int i = 0 ; i < img_dx.rows ; i++){
    for (int j = 0 ; j < img_dx.cols; j++) {
      output.at<float>(i,j ) = atan2(img_dy.at<float>(i,j),  img_dx.at<float>(i,j)); 
    }
  }

}
/* compute gradient and magnitude of input image
Input : img ( 1 - channel )
Output: imgMat (floating point), img_dir (integer)
*/
void ImageObj::compute_grad_mag (const Mat& img, float sigma, Mat& img_mag, Mat& img_dir){  
  vector<float> x ; 
  for (int i = (floor)(-3.0 * sigma + 0.5) ; i <= (floor)(3.0 * sigma + 0.5) ; i++) { 
    x.push_back((float)i); 
  }
  Mat x_sample    = Mat(x); 
  Mat g_ori, x_tmp, x_square;
  cv::pow(x_sample, 2, x_square);
  Mat x_square_2 = (-1)*x_square/(2*sigma*sigma); 
  cv::exp(x_square_2, g_ori); //G = exp(-x.^2/(2*sigma*sigma))
  Scalar acc_gray = sum(g_ori); 
  float  acc        = acc_gray[0];  
  Mat G                  = g_ori/acc;
  Mat out   = x_sample.mul(g_ori); 
  Mat D                  = -2*out/(sqrt(2*M_PI) * pow(sigma,3));
  Mat Dt          = D.t();
  Mat Gt          = G.t();   
  Mat img2      = conv2(img  , D, CONVOLUTION_SAME) ; // img1: convolute img with kernel D
  Mat img1      = conv2(img  ,Dt, CONVOLUTION_SAME); // img2: convolute img with kernel D'
  Mat img_dy   = conv2(img2,Gt, CONVOLUTION_SAME); // img_dx: convolute img1 with kernel G'
  Mat img_dx   = conv2(img1, G, CONVOLUTION_SAME); ; // img_dy: convolute img2 with kernel G

  cv::magnitude(img_dy, img_dx, img_mag); 
  take_atan(img_dx, img_dy, img_dir);
}



/*Matrix power operator */
Mat ImageObj::take_power(const Mat& input_mat, float value){
  Mat w,u,vt;
  SVD::compute(input_mat, w,u,vt); 
  Mat w_modify; 
  cv::pow(w,value, w_modify);
  Mat w_trun   = Mat::eye( u.cols, vt.rows, u.type());
  for (int i = 0 ; i < w_trun.rows ; i++) {
    w_trun.at<float>(i,i) = w_modify.at<float>(i,0); 
  }
  //take the diagonal part of w_modify and put it into a new matrix 
  Mat out_mat = u* w_trun*vt;
  return out_mat; 
} 

/* Perform operation to each element of input_mat
Input:      input_mat 
Output:   out_mat
*/
Mat ImageObj::take_floor(const Mat& input_mat){
  Mat out_mat(input_mat) ; 
  for (int i = 0 ; i < input_mat.rows; i++) {
    for (int j = 0 ; j < input_mat.cols ; j++){
      out_mat.at<float>(i,j) = floor(input_mat.at<float>(i,j)); 
    }
  }
  return out_mat; 
}


/* Replicates the grid vectors xgv and ygv to produce a full grid. 
The output coordinate arryas X and Y contain copies of the grid vectors xgv and ygv respectively.
Input: xgv, ygv: Grid vectors specifying a series of grid point coordinates in the x,y directions, respectively 
*/
void ImageObj::meshgrid(const Mat& xgv, const Mat& ygv, Mat& X, Mat& Y) {
  cv::repeat(xgv.reshape(1,1), ygv.total(), 1, X);
  cv::repeat(ygv.reshape(1,1).t(), 1, xgv.total(), Y); 
}

/* This function equivalent to Matlab's conv2() function*/
Mat ImageObj:: conv2( const Mat& img, const Mat& ikernel, ConvolutionType type) {
  Mat dest; 
  Mat kernel;
  cv::flip(ikernel, kernel, -1);
  Mat source = img; 
  if (CONVOLUTION_FULL == type) {
    source = Mat();
    const int additionalRows = kernel.rows -1, additionalCols = kernel.cols - 1 ; 
    cv::copyMakeBorder(img, source, (additionalRows +1)/2 , additionalRows/2, (additionalCols + 1)/2, additionalCols/2, BORDER_CONSTANT, Scalar(0)); 
  } 
  Point anchor( kernel.cols - kernel.cols/2 - 1, kernel.rows - kernel.rows/2 - 1 );
  int borderMode = BORDER_CONSTANT; 
  cv::filter2D(source, dest, img.depth(), kernel, anchor, 0 , borderMode); 
  if (CONVOLUTION_VALID  == type){
    dest = dest.colRange((kernel.cols-1)/2, dest.cols - kernel.cols/2).rowRange((kernel.rows-1)/2, dest.rows - kernel.rows/2);
  }
  return dest; 
}


/*
The function purifies duplicated features to clearn features by removing redundancy
Input:      input_feat 
Output:   output_feat 
*/
vector<EllipseRegion>  ImageObj::purify_feature(const vector<EllipseRegion> & input_feat){
  int n_dup_feat    = input_feat.size();
  vector<EllipseRegion> output_feat;
  EllipseRegion last_feat(0,0,0,0,0); 
  for (int i = 0 ; i < n_dup_feat; i++ )  {
    EllipseRegion cur_feat = input_feat.at(i);

    if ( last_feat == cur_feat) {continue;}

    EllipseRegion clean_feat(cur_feat.ellipse_u, cur_feat.ellipse_v,cur_feat.ellipse_a, cur_feat.ellipse_b, cur_feat.ellipse_c);
    output_feat.push_back(clean_feat);
    last_feat = cur_feat;
  }
  return output_feat;
}

/* This function generate unique ID for each ellipse feature point */
vector<int> ImageObj::generate_feat_label(const vector<EllipseRegion>& feat_vec){
  int n_feat          = feat_vec.size();
  vector<int> feat_label; feat_label.resize(n_feat); 
  int nClean_feat = 0 ;
  EllipseRegion last_feat(0,0,0,0,0);
  
  for (int i = 0 ; i < n_feat ; i++){
    EllipseRegion cur_feat  = feat_vec.at(i);
    cur_feat.ellipse_ori        = 0; 
    if (last_feat == cur_feat) {
      feat_label.at(i)         = nClean_feat; 
    }else {
      nClean_feat             = nClean_feat + 1 ;
      feat_label.at(i)         = nClean_feat;      
      last_feat                  = cur_feat;
    }
    feat_label.at(i)         = feat_label.at(i) -1;//make the index start from 0
  }
  return feat_label;
}


Mat ImageObj::compute_L2_distance(const Mat& desc_ref, const Mat& desc_tar){
  std::chrono::time_point<std::chrono::system_clock> start, end;
  start       = std::chrono::system_clock::now();
  std::chrono::duration<float> elapsed_seconds;

  int n_feat_ref        = desc_ref.rows;
  int n_feat_tar        = desc_tar.rows;
  Mat aa                   = desc_ref.mul(desc_ref);  
  Mat bb                   = desc_tar.mul(desc_tar);
  Mat aa_summed, bb_summed, ab, aa_tmp, bb_tmp;
  cv::reduce(aa, aa_summed, 1, CV_REDUCE_SUM, CV_32F);
  cv::reduce(bb, bb_summed, 1, CV_REDUCE_SUM, CV_32F);
  ab = desc_ref * desc_tar.t();
  aa_tmp = cv::repeat(aa_summed, 1,  n_feat_tar  );
  bb_tmp = cv::repeat(bb_summed.t(),  n_feat_ref, 1 );
  Mat dist_mat_L2 = aa_tmp + bb_tmp - 2*ab;
  end = std::chrono::system_clock::now();
  elapsed_seconds = end-start;
  cout<<elapsed_seconds.count() <<" sec. elapsed time for L2 distance computation. " <<endl;

  return dist_mat_L2;
}

Mat ImageObj::compute_chi_distance(const Mat& desc_ref, const Mat& desc_tar) {
  std::chrono::time_point<std::chrono::system_clock> start, end;
  start       = std::chrono::system_clock::now();
  std::chrono::duration<float> elapsed_seconds;
  int n_feat_ref        = desc_ref.rows;
  int n_feat_tar        = desc_tar.rows;
  Mat dist_mat_chi(n_feat_ref, n_feat_tar, CV_32F);
  for (int i_feat_ref = 0 ; i_feat_ref < n_feat_ref ; i_feat_ref++){   
     for ( int i_feat_tar = 0 ; i_feat_tar < n_feat_tar ; i_feat_tar++){      
      float acc= cv::compareHist( desc_ref.row(i_feat_ref) ,  desc_tar.row(i_feat_tar), CV_COMP_CHISQR);
      dist_mat_chi.at<float>(i_feat_ref, i_feat_tar) = acc;
    }
  }
  end = std::chrono::system_clock::now();
  elapsed_seconds = end-start;
  cout<<elapsed_seconds.count() <<" sec. elapsed time for chi_square distance computation. " <<endl;

  return dist_mat_chi;
}

Mat ImageObj::compute_chi_distance_fast(const Mat& desc_ref, const Mat& desc_tar) {
      
      std::chrono::time_point<std::chrono::system_clock> start, end;
      start       = std::chrono::system_clock::now();
      std::chrono::duration<float> elapsed_seconds;
      int dim = desc_ref.cols;
      int n1   = desc_ref.rows;
      int n2   = desc_tar.rows; 
     float *K = (float*)malloc(n1*n2*sizeof(float));
      const float mean_K = chi2_distance_float(dim, n1, (float*)desc_ref.data, n2, (float*)desc_tar.data, K);
      Mat dist_mat_chi(n1, n2, CV_32F, K);
      end = std::chrono::system_clock::now();
      elapsed_seconds = end-start;
      cout<<elapsed_seconds.count() <<" sec. elapsed time for chi_square distance computation. " <<endl;
      return dist_mat_chi;
}


/* This function perform matching between two set of descriptors 
Input :   feature descriptor from reference and target image, 
  feature position in target image
Output:  matchlist, matchlist_score
*/ 
void ImageObj:: make_initial_matches() {
  /* Many to one matching*/
  int n_feat_ref        = feat_desc_ref.rows;
  int n_feat_tar        = feat_desc_tar.rows;
  int max_pairs       = n_feat_ref * 500;
  int nPairs              = 0;
  Mat m_plus, m_delta,  m_delta_s, m_plus_s, m_chi;  
  int kbound            = knn;
  int num_selected = 0 ; 
  feat_desc_ref.convertTo(feat_desc_ref, CV_32F);
  feat_desc_tar.convertTo(feat_desc_tar, CV_32F);

  /* For each key-point in the reference image */   
      //Mat dist_mat(n_feat_ref, n_feat_tar, CV_32F);
      Mat dist_mat;
      if ( distance_type_ == "euclidean"){
        dist_mat = compute_L2_distance(feat_desc_ref, feat_desc_tar);      
      }else if (distance_type_ == "chi_square"){
        //dist_mat= compute_chi_distance(feat_desc_ref, feat_desc_tar);
        dist_mat= compute_chi_distance_fast(feat_desc_ref, feat_desc_tar);
      }else{

      }
   
      std::chrono::time_point<std::chrono::system_clock> start, end;
      start             = std::chrono::system_clock::now();
      std::chrono::duration<float> elapsed_seconds;

  for (int i_feat_ref = 0 ; i_feat_ref < n_feat_ref ; i_feat_ref++){   
    
    vector<int>cand_idx;  
    vector<float>cand_score;  
    cand_idx.reserve( n_feat_tar);  
    cand_score.reserve( n_feat_tar );
   
    for ( int i_feat_tar = 0 ; i_feat_tar < n_feat_tar ; i_feat_tar++){      
      float acc = dist_mat.at<float>(i_feat_ref, i_feat_tar);
      cand_idx.push_back(i_feat_tar);
      cand_score.push_back(acc);         
    }
    
    num_selected = n_feat_tar;    
    // Find the k-nn from the candidates 
 
    kbound= min( knn, num_selected); 
    //To select knn candidates 
    for (int itr_i = 0 ; itr_i <kbound ; itr_i++){
      if ( knn < num_selected ){ //for each previous candidate
        for (int itr_j = itr_i ; itr_j < num_selected ; itr_j++) {
          if ( cand_score.at(itr_i) > cand_score.at(itr_j) ){ 
            bool add_feature_flag        = true; 
            /* Compared with the preselected itr_i candidates*/           
            for (int idx = 0 ; idx < itr_i; idx++){ 
              int match_idx_pre  = cand_idx[idx]; 
              int pre_match_x     = ellipse_corners_tar.at(match_idx_pre).ellipse_u;  
              int pre_match_y     = ellipse_corners_tar.at(match_idx_pre).ellipse_v; 
              int cur_idx               = cand_idx[itr_j] ;
              int cur_x                  = ellipse_corners_tar.at(cur_idx).ellipse_u;
              int cur_y                  = ellipse_corners_tar.at(cur_idx).ellipse_v; 
              float dis              = sqrt(( pre_match_x - cur_x) * ( pre_match_x - cur_x) +  ( pre_match_y - cur_y)* ( pre_match_y - cur_y) ); 
              if ( dis < radius_nn ){               
                add_feature_flag = false; 
                break; 
              }
            }
            if ( add_feature_flag == true ){              
              std::swap( cand_score.at(itr_i),  cand_score.at(itr_j) );             
              std::swap( cand_idx.at(itr_i), cand_idx.at(itr_j) );
            }
          }
        }  
      }
    
      //construct match structure
      matchlist.insert(std::make_pair( i_feat_ref, cand_idx[ itr_i])); 
      matchlist_score.push_back(cand_score[itr_i]);  
      nPairs++; 
      if ( nPairs == max_pairs ){
        break;
      }
    }
  }
  
  end = std::chrono::system_clock::now();
  elapsed_seconds = end-start;
  cout<<elapsed_seconds.count() <<" sec. elapsed time for matching " << n_feat_ref<< "-"<<n_feat_tar <<" features. "<<endl;
  if (b_match_distribution ==1  ) {  
    vector<int> pair_matches = map_to_vector(matchlist , matchlist.size()) ; 
    eliminate_matches(pair_matches);    
  }
  
  return;
}

/* 
Given matchlist id, return match feature idx1 and idx2 
*/
void ImageObj::take_match_id(const vector<int> pair_matches, const int matchID, int& idx1, int& idx2){

  int n_match = pair_matches.size() /2;
  idx1        = pair_matches[matchID];
  idx2           = pair_matches[matchID +n_match];
}

/* 
eliminate euivalent matches based on the position, accumulate features accoring to the rank of sqdist (ascending)
*/
void ImageObj:: eliminate_matches(vector<int>& pair_matches){
  // sort the score : from smaller to bigger
  int n_initial_matches                = matchlist.size(); 
  vector<int> tmpair_matches_idx     =  sort_index<float>(matchlist_score) ;    
  vector<int> filter_matches ;
  vector<int> sel_idx ; sel_idx.reserve( n_initial_matches );
   std::chrono::time_point<std::chrono::system_clock> start, end;
  start            = std::chrono::system_clock::now();   
  int num_of_trees = 4;  // 5: optimal
  int flann_precision = 32; //128 high
  if (b_filter_match && n_initial_matches > 0 ){
    // eliminate equvilent matches based on the position 
    // accumulate features according to the rank of sqdist (ascending )
    cv::flann::KDTreeIndexParams indexParams(num_of_trees);
    sel_idx.push_back(tmpair_matches_idx.at(0));  
    int matchIdx            = tmpair_matches_idx.at(0);
    int mIds1, mIds2; 
    take_match_id( pair_matches, matchIdx, mIds1, mIds2);     
    Mat  xy_sel_view1   = (Mat_<float>(1,2) <<ellipse_corners_ref.at( mIds1 ).ellipse_u, ellipse_corners_ref.at( mIds1 ).ellipse_v); 
    Mat  xy_sel_view2   = (Mat_<float>(1,2) <<ellipse_corners_tar.at( mIds2 ).ellipse_u, ellipse_corners_tar.at( mIds2 ).ellipse_v);   
    int n_sel_matches          = 1;
    int count                  = 1; 
    int mIdx1 , mIdx2 ;
    Mat indices1, indices2, dist1, dist2 ;

    for ( int iter_cand = 1 ; iter_cand < n_initial_matches ; iter_cand++) {            
       vector<int> ridx_view1, ridx_view2, equi_ridx;
      take_match_id( pair_matches, tmpair_matches_idx.at(iter_cand), mIdx1, mIdx2 ) ;
      Mat xy_view1                =  (Mat_<float>(1,2) <<ellipse_corners_ref.at( mIdx1 ).ellipse_u, ellipse_corners_ref.at( mIdx1 ).ellipse_v); ;     
      Mat xy_view2                =  (Mat_<float>(1,2) <<ellipse_corners_tar.at( mIdx2 ).ellipse_u, ellipse_corners_tar.at( mIdx2 ).ellipse_v); ; 
      // finds the points within the search radius            
      
      if ( b_use_flann == true ){
          cv::flann::Index kdtree1(xy_sel_view1, indexParams);  
          cv::flann::Index kdtree2(xy_sel_view2, indexParams);
          int num1                        = kdtree1.radiusSearch(xy_view1, indices1,  dist1, redundancy_threshold*redundancy_threshold,  xy_sel_view1.rows, cv::flann::SearchParams(flann_precision));      
          int num2                        = kdtree2.radiusSearch(xy_view2, indices2,  dist2, redundancy_threshold*redundancy_threshold,  xy_sel_view2.rows, cv::flann::SearchParams(flann_precision));                  
          equi_ridx.reserve(max(num1,num2));  
         if ( num1 > 0 && num2 >0 ) { 
              ridx_view1  = Mat_<int>(indices1);
              ridx_view2  = Mat_<int>(indices2);              
              ridx_view1.resize(num1); 
              ridx_view2.resize(num2);      
              std::sort(ridx_view1.begin(), ridx_view1.end());
              std::sort(ridx_view2.begin(), ridx_view2.end());
              std::set_intersection( ridx_view1.begin(), ridx_view1.end(), ridx_view2.begin(), ridx_view2.end(), std::inserter(equi_ridx, equi_ridx.end())); 
         }   
      }else {
          ridx_view1                      = brute_search_wrapper(xy_sel_view1, xy_view1, redundancy_threshold);
          ridx_view2                      = brute_search_wrapper(xy_sel_view2, xy_view2, redundancy_threshold);
          std::set_intersection( ridx_view1.begin(), ridx_view1.end(), ridx_view2.begin(), ridx_view2.end(), std::inserter(equi_ridx, equi_ridx.end())); 
       }
    
      if ( equi_ridx.size() == 0 ) {  
        n_sel_matches              = n_sel_matches + 1 ; 
        //insert it into the selected xy list
         sel_idx.push_back(tmpair_matches_idx.at(iter_cand));
         xy_sel_view1.push_back(xy_view1);
         xy_sel_view2.push_back(xy_view2); 
      }else{
        filter_matches.push_back(iter_cand);
        count = count + 1;        
      } 
      equi_ridx.clear();        
    }
     end = std::chrono::system_clock::now();
    std::chrono::duration<float> elapsed_seconds = end-start;
    cout<< elapsed_seconds.count() <<" sec. elapsed time for eliminate equivalent matches."<<endl;
    
    cout<< "original matchlist size :"<< matchlist.size() <<", candidates selected avoiding equvilent matches : "<< n_sel_matches<< endl;
    //re-align ascending 
    vector<int> sel_index                           = sort_index<int>( sel_idx ); 
    std::multimap<int, int> matchlist_filter= erase_element_from_map(matchlist, sel_idx) ; 
    vector<float> matchlist_score_filter     = erase_element_from_array_int(matchlist_score, sel_idx) ;     
    matchlist                                             = matchlist_filter;
    matchlist_score                                   = matchlist_score_filter;
    }
}

  vector<int> ImageObj::brute_search_wrapper(const Mat& xy_sel_view, const Mat& xy_view, float r_thres){
      float *p   =  (float*)xy_sel_view.data; 
      float *qp = (float*)xy_view.data;
      vector<int> idcv;
      int dim = xy_sel_view.cols; //dimension of points
      int N     =  xy_sel_view.rows;//number of reference points
      BruteRSearch(p,qp ,r_thres,N,dim,&idcv);      
      return idcv;
  }