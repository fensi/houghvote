#include<iostream>
using namespace std;

#include "imageobj.h"
#include "houghvote.h"

int main(int argc, char* argv[]){

  /* Load Image */  
  ImageObj* myobj = new ImageObj(argv[1], argv[2]);
  myobj->load_images(); 
      
  myobj->extract_feature_ref();
  myobj->extract_feature_tar();
  
  myobj->make_initial_matches();
  
  /*Hough Voting */
  HoughVote houghVoter(*myobj);
  houghVoter.do_hough_voting();
  //houghVoter.draw_match(100);

  return 0;
}; 

